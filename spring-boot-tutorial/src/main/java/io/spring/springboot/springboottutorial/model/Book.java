package io.spring.springboot.springboottutorial.model;

public class Book {

	private long id;
	private String author;
	private String title;
	
	public Book(long id, String author, String title) {
		super();
		this.id = id;
		this.author = author;
		this.title = title;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", author=" + author + ", title=" + title + "]";
	}
	
}
