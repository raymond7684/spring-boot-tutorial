package io.spring.springboot.springboottutorial.web;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.spring.springboot.springboottutorial.model.Book;

@RestController
public class BooksController {

	@GetMapping(value="/books")
	public List<Book> getAllBooks() {
		return Arrays.asList(new Book(1l, "Roberto Saviano", "Gomorra"));
	}
}
