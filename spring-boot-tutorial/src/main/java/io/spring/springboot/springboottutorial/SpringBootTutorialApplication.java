package io.spring.springboot.springboottutorial;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBootTutorialApplication {

	private static final Log LOG = LogFactory.getLog(SpringBootTutorialApplication.class);
	
	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(SpringBootTutorialApplication.class, args);
		
		for (String beanDefinitionName: applicationContext.getBeanDefinitionNames()) {
			LOG.info(beanDefinitionName);
		}
	}
}
